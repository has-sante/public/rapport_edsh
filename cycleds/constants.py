import logging
import os
from pathlib import Path
from typing import Final
from dotenv import load_dotenv
import seaborn as sns


load_dotenv()
LOG_LEVEL = os.getenv("LOG_LEVEL", "INFO")
logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


DATA_DIRNAME = "data"
REPORT_DIRNAME = "reports"

PATH_ROOT: Final[Path] = Path(__file__).parent.parent.absolute()
DIR2DATA: Final[Path] = PATH_ROOT / DATA_DIRNAME
PATH2DATA_COLLECTED: Final[Path] = DIR2DATA / "cycle_eds"
DIR2DATA_RESOURCES: Final[Path] = DIR2DATA / "resources"

DIR2REPORTS: Final[Path] = PATH_ROOT / REPORT_DIRNAME
DIR2IMAGES: Final[Path] = DIR2REPORTS / "images"

# Locally defined
DIR2PAPER_IMG: Path = Path(
    os.getenv("DIR2PAPER_IMG", "../papier-cycle-eds/figures/")
)
DIR2PAPER_TABLES: Path = Path(
    os.getenv("DIR2PAPER_TABLES", "../papier-cycle-eds/tables/")
)

CACHE_LOCATION = "~/.cachedir"
# Plotting
sns.set_theme(style="whitegrid")
