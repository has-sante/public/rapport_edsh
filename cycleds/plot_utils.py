from pathlib import Path
import seaborn as sns
from matplotlib import cm
import matplotlib.pyplot as plt
import geopandas as gpd

from cycleds.constants import DIR2IMAGES, DIR2PAPER_IMG

sns.set_context("talk")
sns.set(font_scale=2)
sns.set_style("whitegrid")
plt.rcParams.update(
    {
        "figure.figsize": (14, 6),
        # "font.size": 20,
        # "text.usetex": True,
        # "text.latex.preamble": r"\usepackage{amsfonts}",
    }
)

# Colors
TAB_COLORS_20 = [cm.tab20(i) for i in range(20)]


def barplot_change_width(ax, new_value):
    """Change seaborn barplot widths, [source](https://stackoverflow.com/questions/34888058/changing-width-of-bars-in-bar-chart-created-using-seaborn-factorplot)

    Args:
        ax (_type_): _description_
        new_value (_type_): _description_
    """
    for patch in ax.patches:
        current_width = patch.get_width()
        diff = current_width - new_value

        # we change the bar width
        patch.set_width(new_value)

        # we recenter the bar
        patch.set_x(patch.get_x() + diff * 0.5)


def save_figure_to_folders(
    figure_name: Path,
    images_dir=True,
    paper_dir=True,
    pdf=True,
):
    if type(figure_name) != Path:
        figure_name = Path(figure_name)
    if images_dir:
        (DIR2IMAGES).mkdir(exist_ok=True, parents=True)
        plt.savefig(
            str(DIR2IMAGES / f"{figure_name.name}.png"),
            bbox_inches="tight",
        )
        if pdf:
            plt.savefig(
                str(DIR2IMAGES / f"{figure_name.name}.pdf"),
                bbox_inches="tight",
            )
    if paper_dir:
        (DIR2PAPER_IMG).mkdir(exist_ok=True, parents=True)
        plt.savefig(
            str(DIR2PAPER_IMG / f"{figure_name.name}.png"),
            bbox_inches="tight",
        )
        if pdf:
            plt.suptitle("")
            plt.savefig(
                str(DIR2PAPER_IMG / f"{figure_name.name}.pdf"),
                bbox_inches="tight",
            )


def annotate_axes(ax, text, fontsize=18):
    ax.text(
        0.5,
        0.5,
        text,
        transform=ax.transAxes,
        ha="center",
        va="center",
        fontsize=fontsize,
        color="darkgrey",
    )


def annotate_from_gpd(
    ax, g_df: gpd.GeoDataFrame, label_column: str, size: int = 14
):
    for x, y, label in zip(
        g_df.geometry.x, g_df.geometry.y, g_df[label_column]
    ):
        ax.annotate(
            label,
            xy=(x, y),
            xytext=(-20, 6),
            textcoords="offset points",
            size=size,
        )
