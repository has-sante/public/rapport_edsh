from numpy import apply_along_axis
import pandas as pd
import geopandas as gpd
import logging

from cycleds.constants import DIR2DATA_RESOURCES, DIR2DATA_RESOURCES
from cycleds.data.processing_utils import (
    COL_FINESS_EJ,
    COL_FINESS_GEO,
    FINESS_TYPES,
)
from ..plot_utils import TAB_COLORS_20

logger = logging.getLogger(__name__)


def get_finess():
    """
    Nous utilisons le fichier finess préprocessé par le [projet scope santé](https://gitlab.has-sante.fr/has-sante/public/alpha/scope-sante-data).
    [Documentation des variables sur datagouv.fr](https://www.data.gouv.fr/fr/datasets/base-sur-la-qualite-et-la-securite-des-soins-anciennement-scope-sante/)

    Le finess est originellement disponible à cette adresse : https://www.data.gouv.fr/fr/datasets/finess-extraction-du-fichier-des-etablissements/
    """
    finess = pd.read_csv(DIR2DATA_RESOURCES / "finess.csv").rename(
        columns={
            "num_finess_et": COL_FINESS_GEO,
            "num_finess_ej": COL_FINESS_EJ,
        }
    )
    return finess


def get_sae(year: int = 2019) -> pd.DataFrame:

    df_sae = pd.read_csv(DIR2DATA_RESOURCES / "sae.csv")
    df_sae[FINESS_TYPES] = df_sae[FINESS_TYPES].astype("str")
    if year is not None:
        df_sae = df_sae[df_sae["id_an"] == year]
    years = df_sae["id_an"].unique()
    logger.info(f"Utilisation de la SAE des années : {years}")
    return df_sae


# adding color and alpha for the plot
# TYPE_ETABLISSEMENT2COLOR = {
#     "CHU": TAB_COLORS_20[0],
#     "CH": TAB_COLORS_20[1],
#     "Public": TAB_COLORS_20[4],
#     "Privé à but non lucratif": TAB_COLORS_20[8],
#     "CLCC": TAB_COLORS_20[10],
#     "Non interrogé": "white",
# }
AVANCEMENT2COLOR = {
    "experimentation": {
        "color": TAB_COLORS_20[0],
        "label": "Pas d'information",
    },
    "production": {"color": TAB_COLORS_20[4], "label": "En production"},
    "prospective": {"color": TAB_COLORS_20[8], "label": "Prospectif"},
    "no_eds": {"color": (0, 0, 0, 1), "label": "Pas d'EDS"},
    None: {"color": TAB_COLORS_20[15], "label": "Pas d'information"},
}
entrepot_state_mapping = {
    None: "Pas d'information",
    "experimentation": "Experimentation",
    "prospective": "Prospectif",
    "no_eds": "Pas d'EDS",
    "production": "En production",
}


def ajout_coordonnees_geo(df_hospital: pd.DataFrame) -> gpd.GeoDataFrame:
    """
    Retourne les coordonnées géographiques et les labels à afficher des organisation sous-jacentes aux entrepôts.

    Pour les organisations rassemblant plusieurs centres, un finess géographique arbitraire et raisonnable (au centre de l'agglomération concernée) a été selectionné à la main. C'est ce centre qui donne la position sur la carte.

    - Attend une colonne FINESS_TYPE_GEO contenant le numéro de finess géographique

    """
    col_of_interest = [
        "raison_sociale_longue_et",
        "date_export",
        "type_etablissement",
        "latitude",
        "longitude",
        "libelle_region",
        "code_officiel_geo",
        "source_coord_et",
        COL_FINESS_GEO,
    ]
    finess = get_finess()
    # ajout des CHUs non interrogés
    chu_non_interroges = pd.DataFrame(
        [
            ["CHU Caen", "140004383"],
            ["CHR\nMetz-Thionville", "570026682"],
            ["CHU Strasbourg", "670783273"],
            ["CHU Reims ", "510002454"],
            ["CHU Besançon", "250006954"],
            ["CHU\nDijon", "210987558"],
            ["CHR Orléans", "450002613"],
            ["CHU\nClermont-Ferrand", "630783538"],
            ["CHU\nSaint-Etienne", "420782567"],
            ["CHU Limoges", "870018181"],
            ["CHU Nîmes", "300782117"],
            ["CHU Nice", "060785003"],
            ["CHU La Réunion", "970400081"],
            ["CHU Fort de France", "970211207"],
            [
                "CHU Pointe à Pitre",
                "970100442",
            ],  # fake for martinique and corse which are buggy
            ["CH Ajaccio", "2A0022778"],
        ],
        columns=["organisation", COL_FINESS_GEO],
    )
    chu_non_interroges["interroges"] = "X"
    hospital_w_coordinates = (
        df_hospital.merge(
            chu_non_interroges[[COL_FINESS_GEO, "interroges"]],
            how="left",
            on=COL_FINESS_GEO,
        )
        .merge(
            finess[col_of_interest],
            on=COL_FINESS_GEO,
            how="inner",
        )
        .sort_values([COL_FINESS_GEO, "date_export"], ascending=False)
        .groupby(COL_FINESS_GEO)
        .agg("first")
        .reset_index()
        .fillna({"interroges": "o"})
    )
    breakpoint()
    # TODO: verify that this should be removed
    # def map2chu(organisation: str, type_etablissement: str):
    #     if organisation in ["APHM", "CHU Strasbourg", "HCL"]:
    #         return "CHU"
    #     elif organisation in ["HEGP", "Institut Imagine, Necker"]:
    #         return "Public"
    #     else:
    #         return type_etablissement

    # hospital_w_coordinates = hospital_w_coordinates.assign(
    #     type_etablissement=lambda df: df.apply(
    #         lambda x: map2chu(x["organisation"], x["type_etablissement"]),
    #         axis=1,
    #     )
    # )
    entrepot_state_label = "Etat d'avancement"
    hospital_w_coordinates = hospital_w_coordinates.rename(
        columns={"entrepot_state": entrepot_state_label}
    )

    hospital_w_coordinates["color"] = hospital_w_coordinates[
        entrepot_state_label
    ].apply(lambda x: AVANCEMENT2COLOR[x]["color"])

    hospital_w_coordinates[entrepot_state_label] = hospital_w_coordinates[
        entrepot_state_label
    ].apply(lambda x: AVANCEMENT2COLOR[x]["label"])

    # hospital_w_coordinates = hospital_w_coordinates.assign(
    #     color=lambda df: df["type_etablissement"].apply(
    #         lambda x: TYPE_ETABLISSEMENT2COLOR[x]
    #     )
    # )
    # hospital_w_coordinates = hospital_w_coordinates.assign(
    #     color=lambda df: df.apply(
    #         lambda x: "white" if x["interroges"] != 1 else x["color"], axis=1
    #     )
    # )
    gpd_hospitals = gpd.GeoDataFrame(
        hospital_w_coordinates,
        geometry=gpd.points_from_xy(
            hospital_w_coordinates.longitude,
            hospital_w_coordinates.latitude,
        ),
    )
    return gpd_hospitals


def ajout_sae_n_sejours(df_entrepots: pd.DataFrame) -> pd.DataFrame:
    """
    Ajoute le nombre de séjours MCO en hospitalisation complète.

    Nous utilisons la SAE préprocessée par le [projet scope santé](https://gitlab.has-sante.fr/has-sante/public/alpha/scope-sante-data).
    [Documentation des variables sur datagouv.fr](https://data.drees.solidarites-sante.gouv.fr/api/datasets/1.0/708_bases-statistiques-sae/attachments/sae_2019_dictionnaires_des_variables_par_borderaux_xlsx)
    Args:
        df_entrepot (pd.DataFrame): Données collectées manuellement sur les entrepôts.
    """

    mask_geo = df_entrepots[COL_FINESS_EJ].isna()
    df_entrepots.loc[~mask_geo, "finess_type"] = COL_FINESS_EJ
    df_entrepots.loc[mask_geo, "finess_type"] = COL_FINESS_GEO

    df_sae_last_year = get_sae()

    cols_sejours = ["mco_sejhc_mco"]
    entrepots_w_finess = []
    for finess_type in [
        COL_FINESS_EJ,
        COL_FINESS_GEO,
    ]:

        entrepots_by_finess_type = df_entrepots.loc[
            df_entrepots["finess_type"] == finess_type
        ]
        sae_by_finess_type = (
            df_sae_last_year[[finess_type, *cols_sejours]]
            .groupby(finess_type)
            .agg("sum")
        ).reset_index()

        sae_by_finess_type["n_sejours_comptes"] = sae_by_finess_type[
            cols_sejours
        ].sum(axis=1)

        entrepots_w_finess.append(
            entrepots_by_finess_type.merge(
                sae_by_finess_type[[finess_type, "n_sejours_comptes"]],
                on=finess_type,
                how="inner",
            )
        )

    entrepots_wo_finess = df_entrepots.loc[df_entrepots["finess_type"].isna()]
    logger.info(
        f"Les données de la sae n'ont pas été ajoutés pour les entrepôts suivants: {entrepots_wo_finess['entrepot_id']}"
    )
    return pd.concat([*entrepots_w_finess, entrepots_wo_finess], axis=0)
