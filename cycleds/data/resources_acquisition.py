"""Collect external resources
"""
import logging
from typing import Final
import pandas as pd

from cycleds.constants import PATH2DATA_COLLECTED, LOG_LEVEL

logger: Final[logging.Logger] = logging.getLogger(__name__)

logger.setLevel(LOG_LEVEL)


def download_specialites_snsd(save=True) -> pd.DataFrame:
    """Download from https://gitlab.has-sante.fr/healthdatahub/schema-snds/ the up to date speciality mapping.
    Add a column to be hand-filled

    Returns:
        pd.DataFrame: _description_
    """
    # download raw IR_SPA_D mapping from
    ir_spa_d_url = "https://gitlab.has-sante.fr/healthdatahub/schema-snds/-/raw/master/nomenclatures/ORAVAL/IR_SPA_D.csv"
    logger.info(
        f"Télécharge les données de spécialités du SNDS à {ir_spa_d_url}"
    )
    ir_spa_d = pd.read_csv(ir_spa_d_url, sep=";")

    mapping_specialites = (
        ir_spa_d.copy()
        .assign(
            snds_specialite_label=ir_spa_d["PFS_SPA_LIB"]
            .str.lower()
            .str.capitalize(),
            local_specialite_label="",
        )
        .drop("PFS_SPA_LIB", axis=1)
    )
    path2mapping_specialites = PATH2DATA_COLLECTED / "mapping_specialites.csv"
    if not path2mapping_specialites.exists() and save:
        mapping_specialites.to_csv(path2mapping_specialites, index=False)
        logger.info(
            f"Sauvegarde les spécialités du SNDS dans {path2mapping_specialites}"
        )
    else:
        logger.info(f"{path2mapping_specialites} existe déjà.")
    return mapping_specialites
