import logging
import re
import pandas as pd
from cycleds.constants import PATH2DATA_COLLECTED

logger = logging.getLogger(__name__)


# Resource columns
COL_FINESS_EJ = "id_fi_ej"
COL_FINESS_GEO = "id_fi"

FINESS_TYPES = [COL_FINESS_EJ, COL_FINESS_GEO]

# Data Columns
COL_INTERVIEW_DATETIME = "Date interview"
COL_EQUIPE = "equipe_categorie"
COL_IS_CHU = "is_chu"
COL_ENTREPOT_ID = "entrepot_id"


def get_entrepots() -> pd.DataFrame:
    """Charge la table brute des entrepôts de données."""
    df_entrepots = pd.read_csv(
        PATH2DATA_COLLECTED / "cycle_eds_entrepots.csv",
        dtype={COL_FINESS_GEO: str, COL_FINESS_EJ: str},
    ).dropna(how="all")
    # supprime l'INCA car ce n'est pas un entrepôt clinique
    df_entrepots = df_entrepots.loc[
        df_entrepots[COL_ENTREPOT_ID] != "EDS_INCA"
    ]
    df_entrepots["etablissement_rencontre"] = df_entrepots[
        "etablissement_rencontre"
    ].apply(lambda x: True if x == "checked" else False)
    return df_entrepots


def get_invites() -> pd.DataFrame:
    """Charge la table brute des intervenants du cycle EDS."""
    df_invites = pd.read_csv(
        PATH2DATA_COLLECTED / "cycle_eds_intervenants.csv"
    )
    df_invites[COL_INTERVIEW_DATETIME] = pd.to_datetime(
        df_invites[COL_INTERVIEW_DATETIME], dayfirst=True
    )
    df_invites.loc[df_invites[COL_EQUIPE].isna()] = ""
    return df_invites


def get_etudes() -> pd.DataFrame:
    """Charge la table brute des études de recherche en cours sur les entrepôts."""
    df_entrepots = pd.read_csv(PATH2DATA_COLLECTED / "cycle_eds_etudes.csv")
    return df_entrepots


def map_etudes_to_snds_spes(
    etudes_en_cours: pd.DataFrame, language="FR"
) -> pd.DataFrame:
    """Mappe les domaines médicaux locaux vers les spécialités SNDS.

    Args:
        etudes_en_cours (pd.DataFrame):
    """
    mapping2specialites_df = pd.read_csv(
        PATH2DATA_COLLECTED / "mapping_specialites.csv"
    )
    if language == "FR":
        final_label_col = "snds_specialite_label"
    elif language == "EN":
        final_label_col = "snds_specialite_label_en"
    else:
        ValueError("Only FR and EN supported")
    mapping2specialites_dic = dict(
        zip(
            mapping2specialites_df["local_specialite_label"],
            mapping2specialites_df[final_label_col],
        )
    )
    etudes_en_cours_avec_snds_spes = etudes_en_cours.copy()
    etudes_en_cours_avec_snds_spes[final_label_col] = etudes_en_cours[
        "domaine_medical"
    ].map(lambda x: mapping2specialites_dic.get(x, x))
    missing_keys_in_mapping = set(
        etudes_en_cours["domaine_medical"]
    ).difference(set(mapping2specialites_dic.keys()))
    logger.info(
        f"Les spécialités locales non mappées sont:\n {missing_keys_in_mapping}"
    )
    return etudes_en_cours_avec_snds_spes


def is_chu(df: pd.DataFrame) -> pd.DataFrame:
    df_c = df.copy()
    df_c[COL_IS_CHU] = df_c["organisation"].apply(
        lambda x: True
        if re.search("CHU|CHRU|CHR|APHP|APHM|HCL", x) is not None
        else False
    )
    return df_c
