import logging
import re
from typing import List
import numpy as np
import pandas as pd

from cycleds.constants import CACHE_LOCATION

from joblib import Memory
from nltk.metrics.distance import edit_distance as lv

memory = Memory(CACHE_LOCATION, verbose=0)


def get_topics_in_project(
    df_projets: pd.DataFrame,
    topic: str,
    topic_dict: List[str],
) -> pd.DataFrame:
    """
    Flag a topic by key-word list in targeted columns of the df projects by compiling the regex.

    Parameters
    ----------
    df_projets : pd.DataFrame
        _description_
    topic : str
        _description_
    topic_dict : List[str]
        _description_

    Returns
    -------
    pd.DataFrame
        _description_
    """
    targeted_cols = ["Titre.projet", "Demarche.suivie"]
    reg = re.compile("|".join(topic_dict))
    topic_found_by_cols = (
        np.stack(
            [
                df_projets[col].apply(
                    lambda x: reg.search(x.lower()) is not None
                )
                for col in targeted_cols
            ],
            axis=1,
        ).sum(axis=1)
        >= 1
    )
    logging.info(f"{topic} found in {topic_found_by_cols.sum()} projects")
    return df_projets.loc[topic_found_by_cols]


@memory.cache()
def levenstein_matrix(col_1: np.array, col_2: np.array):
    """Compute levensthein distance between two arrays of string. Cache results with joblib.

    Parameters
    ----------
    col_1 : np.array
        _description_
    col_2 : np.array
        _description_

    Returns
    -------
    _type_
        _description_
    """
    levenstein_m = np.array(len(col_1), len(col_2))
    for i, x in enumerate(col_1):
        for j, y in enumerate(col_2):
            levenstein_m[i, j] = lv(col_1, col_2)
    return levenstein_m


def get_closest_levenstein(word: str, quandidates: np.array):
    if type(quandidates) != np.array:
        quandidates_ = np.array(quandidates)
    levenstein_d = []
    for q in quandidates_:
        levenstein_d.append(lv(word, q))
    best_quandidates = quandidates_[np.argsort(levenstein_d)]
    return best_quandidates
