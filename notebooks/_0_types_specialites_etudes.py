"""
Ce notebook détaille les types et les méthodologies des études menées sur les entrepôts de données de santé. 

Les données ont été récoltées lors du cycle d'échange sur les entrepôts de données hospitaliers menées par la HAS.

Nous étudions principalement la table `cycle_eds_etudes_en_cours.csv` qui référence les études, et leurs objectifs pour les différents entrepôts pour lesquels nous avons pu avoir accès à cette information.

Les types et méthodologies d'études ont été progressivement créés par Adeline Degresmont et Matthieu Doutreligne en s'inspirant des différents types de réutilisations listés par OHDSI ainsi qu'à la lecture des études en cours sur l'EDS de l'APHP.
"""
# %%
from cycleds.constants import (
    PATH2DATA_COLLECTED,
    DIR2IMAGES,
    DIR2REPORTS,
)
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from cycleds.data.processing_utils import (
    COL_ENTREPOT_ID,
    COL_IS_CHU,
    get_entrepots,
    is_chu,
    map_etudes_to_snds_spes,
)
from cycleds.plot_utils import TAB_COLORS_20, save_figure_to_folders

LANGUAGE = "EN"
# %% [markdown] Préparation des données
# %%
etudes_en_cours = pd.read_csv(
    PATH2DATA_COLLECTED / "cycle_eds_etudes.csv", sep=","
).dropna(how="all")
types_etudes = pd.read_json(
    PATH2DATA_COLLECTED / "types_etudes.json", orient="column"
)

entrepots = get_entrepots()
entrepots = is_chu(entrepots)
chu_interviewed = entrepots.loc[
    (~entrepots["entrepot_state"].isna())
    & (entrepots["entrepot_state"] != "no_eds")
    & (entrepots[COL_IS_CHU] == True)
    & (entrepots["etablissement_rencontre"] == True)
]

etudes_en_cours = map_etudes_to_snds_spes(
    etudes_en_cours, language=LANGUAGE
).merge(chu_interviewed[COL_ENTREPOT_ID], on=COL_ENTREPOT_ID, how="inner")
n_etudes = etudes_en_cours.shape[0]
n_entrepots_w_study = etudes_en_cours[COL_ENTREPOT_ID].nunique()
print(n_entrepots_w_study)
category_dummies = (
    pd.get_dummies(etudes_en_cours["type_etude"].explode())
    .groupby(level=0)
    .max()
)
PREFIX_CATEGORY = "type__"
category_dummies.columns = [PREFIX_CATEGORY + col for col in category_dummies]
cols_etude_categories = category_dummies.columns
etudes_w_categories = pd.concat([etudes_en_cours, category_dummies], axis=1)

if LANGUAGE == "EN":
    label_pourcentage = "Percentage"
    label_x = "Percentage of studies"
    label_specialite = "Medical specialty"
    label_number_of_studies = "Number of ongoing studies"
    label_total_studies = f"Total of {n_etudes} studies"
    label_type = "Type of study"
else:
    label_pourcentage = "Pourcentage"
    label_x = "Pourcentage d'études"
    label_specialite = "Spécialité"
    label_number_of_studies = "Nombre d'études en cours"
    label_total_studies = f"Total de {n_etudes} études"
    label_type = "Type d'étude"
# %% [markdown] # Nombre d'études par type d'objectifs :
# Nombre d'études par type:
pourcentage_types_etudes = pd.DataFrame(
    100 * etudes_w_categories[cols_etude_categories].mean(axis=0),
).reset_index()
pourcentage_types_etudes.columns = ["type_etude", label_pourcentage]
pourcentage_types_etudes.loc[:, "type_etude"] = pourcentage_types_etudes.loc[
    :, "type_etude"
].map(lambda x: x.replace(PREFIX_CATEGORY, ""))

pourcentage_types_etudes_final = pourcentage_types_etudes.merge(
    types_etudes, on="type_etude", validate="1:1"
).sort_values(label_pourcentage, ascending=False)
pourcentage_types_etudes_final["type_etude"] = pourcentage_types_etudes_final["type_etude"].map(lambda x: "Diagnostic and prognostic\nalgorithms" if x=="Decision Algorithm" else x)
# %%
# Saving and plotting
pourcentage_types_etudes_final.to_csv(
    DIR2REPORTS / "pourcentage_types_etudes.csv", index=False
)
perc_minimum_type_etude = 1
pourcentage_types_etudes_to_plot = pourcentage_types_etudes_final.loc[
    pourcentage_types_etudes_final[label_pourcentage]
    >= perc_minimum_type_etude
]
fig, ax = plt.subplots(1, 1, figsize=(12, 9))
if LANGUAGE == "EN":
    y_labels = pourcentage_types_etudes_to_plot["type_etude"]
else:
    y_labels = [
        x.replace("_", " ").capitalize()
        for x in pourcentage_types_etudes_to_plot["type_etude_fr"]
    ]
y_pos = np.arange(len(y_labels))
ax.barh(
    y=y_pos,
    width=pourcentage_types_etudes_to_plot[label_pourcentage],
    color=TAB_COLORS_20[: len(pourcentage_types_etudes_to_plot)],
)
plt.gca().invert_yaxis()
fmt = "%.0f%%"  # Format you want the ticks, e.g. '40%'
yticks = mtick.FormatStrFormatter(fmt)
ax.xaxis.set_major_formatter(yticks)
plt.yticks(y_pos, y_labels)
ax.grid(False, axis="y")
ax.set(
    ylabel=label_type,
)
if perc_minimum_type_etude > 0:
    ax.set(xlabel=f"{label_x} (>={perc_minimum_type_etude}%)")
# ax.set_xticklabels(ax.get_xticklabels(), rotation=50)
# plt.suptitle(    f"Objectif poursuivi par chaque étude en cours\néchantillon de {n_etudes} études")
plt.tight_layout()
# breaks the figure size
ax.text(
    x=0.45,
    y=0.95,
    fontdict={"size": 25},
    s=label_total_studies,
    transform=plt.gcf().transFigure,
)
save_figure_to_folders(figure_name=DIR2IMAGES / f"pourcentage_types_etudes_{LANGUAGE}")


# %% [markdown] # Nombre d'études par spécialité médicale :
# %%
if LANGUAGE == "EN":
    specialty_col = "snds_specialite_label_en"
else:
    specialty_col = "snds_specialite_label"
n_etudes_par_specialite = (
    etudes_en_cours[specialty_col].value_counts().sort_values(ascending=False)
)
n_etudes_par_specialite_df = pd.DataFrame(
    {
        label_specialite: n_etudes_par_specialite.index,
        label_number_of_studies: n_etudes_par_specialite.values,
        label_pourcentage: n_etudes_par_specialite.values
        / etudes_en_cours.shape[0]
        * 100,
    }
)
n_etudes_par_specialite_df.to_csv(
    DIR2REPORTS / "pourcentage_etudes_par_specialite.csv", index=False
)
## Il y a trop de spécialités brutes : n_spé=40, on va essayer de mapper aux spécialités SNDS.
# %%
for perc_minimum_specialites in [0, 3]:
    n_etudes_par_specialite_df_to_plot = n_etudes_par_specialite_df.loc[
        n_etudes_par_specialite_df[label_pourcentage]
        >= perc_minimum_specialites
    ]
    if perc_minimum_specialites == 0:
        fig_y = 14
    else:
        fig_y = 8
    fig, ax = plt.subplots(1, 1, figsize=(14, fig_y))
    y_labels = n_etudes_par_specialite_df_to_plot[label_specialite]
    y_pos = np.arange(len(y_labels))
    ax.barh(
        y=y_pos,
        width=n_etudes_par_specialite_df_to_plot[label_pourcentage],
        color=TAB_COLORS_20[: len(n_etudes_par_specialite_df_to_plot)],
    )
    plt.gca().invert_yaxis()
    fmt = "%.0f%%"  # Format you want the ticks, e.g. '40%'
    yticks = mtick.FormatStrFormatter(fmt)
    ax.xaxis.set_major_formatter(yticks)
    if perc_minimum_specialites > 0:
        ax.set_xlabel(f"{label_x} (>={perc_minimum_specialites}%)")
    ax.set_ylabel(label_specialite)
    plt.yticks(y_pos, y_labels)
    # plt.suptitle(f"Spécialité de l'investigateur principal pour chaque étude en cours\néchantillon de {n_etudes} études")
    plt.tight_layout()
    ax.grid(False, axis="y")
    ax.text(
        x=0.6,
        y=0.95,
        fontdict={"size": 25},
        s=label_total_studies,
        transform=plt.gcf().transFigure,
    )
    save_figure_to_folders(
        figure_name=DIR2IMAGES
        / f"pourcentage_etudes_par_specialite_min_perc{perc_minimum_specialites}"
    )

# %% [markdown] # Nombre d'études par Entrepôt :

x_label = "Nombres d'études dans l'échantillon"
n_etudes_par_entrepots = (
    (
        etudes_en_cours.merge(get_entrepots(), on="entrepot_id", how="left")
        .groupby("organisation")["titre_etude"]
        .count()
    )
    .reset_index()
    .rename(columns={"titre_etude": x_label})
).sort_values(x_label, ascending=False)
fig, ax = plt.subplots(1, 1, figsize=(14, 14))
y_labels = n_etudes_par_entrepots["organisation"]
y_pos = np.arange(len(y_labels))
ax.barh(
    y=y_pos,
    width=n_etudes_par_entrepots[x_label],
    color=TAB_COLORS_20[: len(n_etudes_par_entrepots)],
)
plt.gca().invert_yaxis()
ax.text(
    x=0.69,
    y=0.92,
    fontdict={"size": 25},
    s=f"Total de {n_etudes} études",
    transform=plt.gcf().transFigure,
)
ax.set_ylabel("Organisation")
ax.set_xlabel(x_label)
plt.yticks(y_pos, y_labels)
plt.tight_layout()
save_figure_to_folders(
    figure_name=DIR2IMAGES / "nombre_etudes_par_organisation"
)

# %%
