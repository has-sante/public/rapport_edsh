"""
Ce notebook détaille l'historique de la création des entrepôts en traçant un graphique de type timeline.

Les données ont été récoltées lors du cycle d'échange sur les entrepôts de données hospitaliers menées par la HAS.

Nous étudions principalement la table `cycle_eds_entrepots.csv` qui référence les différents entrepôts et leurs caractéristiques.
"""
# %%
from matplotlib.lines import Line2D
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from cycleds.constants import DIR2IMAGES
from cycleds.data.processing_utils import (
    COL_INTERVIEW_DATETIME,
    COL_IS_CHU,
    get_entrepots,
    is_chu,
)
import matplotlib.dates as mdates
from datetime import datetime

from cycleds.plot_utils import save_figure_to_folders

# %%
entrepots = get_entrepots()
entrepots = is_chu(entrepots)
n_chus_total = entrepots[COL_IS_CHU].sum()
print(f"{n_chus_total} entrepôts de CHUs / {entrepots.shape[0]} entrepôts")
print(
    f"Entrepôts non CHU: {entrepots.loc[~entrepots[COL_IS_CHU], 'organisation'].values}"
)
taille_equipe_entrepot_description = entrepots["n_etp_entrepot"].describe(
    percentiles=[0.5]
)
print(f"Taille d'équipe entrepôt:\n {taille_equipe_entrepot_description}")
# %%

language = "EN"
if language == "EN":
    # look only at CHUs
    entrepots = entrepots.loc[
        (~entrepots["entrepot_state"].isna())
        & (entrepots["entrepot_state"] != "no_eds")
        & (entrepots[COL_IS_CHU] == True)
        & (entrepots["etablissement_rencontre"] == True)
    ]
# %% timeline plot
DATE_REF_CNIL = datetime.strptime("2021-11-17", "%Y-%m-%d")
fig, ax = plt.subplots(figsize=(14, 6), constrained_layout=True)
# ax.set(title="Chronologie de mise en place des EDS")
levels_up = np.tile(
    [1, 2, 3, 4],
    int(np.ceil(len(entrepots["date_debut_cnil"].dropna()) / 4)),
)[: len(entrepots["date_debut_cnil"].dropna())]
levels_down = np.tile(
    [-5, -4, -3, -2, -1],
    int(np.ceil(len(entrepots["date_debut"].dropna()) / 5)),
)[: len(entrepots["date_debut"].dropna())]
# plot dates de début:
for date_col, color, levels in zip(
    ["date_debut", "date_debut_cnil"],
    ["blue", "green"],
    [levels_down, levels_up],
):
    entrepots_dates = (
        entrepots[["organisation", date_col]]
        .sort_values(date_col)
        .dropna(axis=0)
        .astype({date_col: "datetime64[ns]"})
    )

    ax.vlines(
        entrepots_dates[date_col], 0, levels, color=f"tab:{color}"
    )  # The vertical stems.
    ax.plot(
        entrepots_dates[date_col],
        np.zeros_like(entrepots_dates[date_col]),
        "-o",
        color="k",
        markerfacecolor="w",
    )  # Baseline and markers on it.

    # annotate lines
    for d, l, r in zip(
        entrepots_dates[date_col],
        levels,
        entrepots_dates["organisation"],
    ):
        if len(r) >= 20:
            r = "\n".join(r.split(","))
        rotation = 0 if l < 0 else 90
        xtext = -2 if l < 0 else 0
        horizontalalignment = "right" if l < 0 else "center"
        ax.annotate(
            r,
            xy=(d, l),
            xytext=(xtext, np.sign(l) * 3),
            textcoords="offset points",
            horizontalalignment=horizontalalignment,
            verticalalignment="bottom" if l > 0 else "top",
            rotation=rotation,
            fontsize=15,
        )

# format xaxis with 4 month intervals
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=12))
ax.xaxis.set_major_formatter(mdates.DateFormatter("%b %Y"))

ax.vlines([DATE_REF_CNIL], -5, 5, color="red", linestyles="dashed")
ax.hlines(
    0, xmin=min(entrepots_dates[date_col]), xmax=DATE_REF_CNIL, color="black"
)
if language:
    ref_cnil_label = "HDW legal baseline\n(CNIL)"
else:
    ref_cnil_label = "Réferentiel CNIL EDS"
ax.annotate(
    ref_cnil_label,
    xy=(DATE_REF_CNIL, -5),
    xytext=(0, -3),
    textcoords="offset points",
    horizontalalignment="center",
    verticalalignment="top",
    weight="bold",
    fontsize=15,
)
plt.setp(ax.get_xticklabels(), rotation=30, ha="right")

handles = [
    Line2D([0], [0], color="blue"),
    Line2D(
        [0],
        [0],
        color="green",
    ),
]
if language:
    labels = ["First works", "Regulatory Autorization (CNIL)"]
    title = "HDW Timeline"
else:
    labels = ["Premiers travaux", "Autorisation CNIL"]
    title = "Historique EDS"
legend_ = plt.legend(
    handles,
    labels,
    title=title,
    loc="upper left",
    prop={"size": 16}
    # bbox_to_anchor=(0.1, 0.9)
)
ax.add_artist(legend_)


# remove y axis and spines
ax.yaxis.set_visible(False)
ax.spines[["left", "top", "right"]].set_visible(False)

ax.margins(y=0.1)
save_figure_to_folders(figure_name=DIR2IMAGES / "timeline_eds")

# %%
