"""
Ce notebook convertit les tables de résultats en csv et excel pour publication. 

"""
# %%
from cycleds.constants import (
    DIR2PAPER_TABLES,
)
import pandas as pd
import shutil
from cycleds.data.processing_utils import (
    get_entrepots,
    get_etudes,
    get_invites,
)

file_loader_dic = {
    "entrepots": get_entrepots,
    "etudes_sur_entrepots": get_etudes,
    "intervenants": get_invites,
}

for file_name, file_loader in file_loader_dic.items():
    df = file_loader()
    df.to_excel(DIR2PAPER_TABLES / f"{file_name}.xlsx", index=False)
    df.to_csv(DIR2PAPER_TABLES / f"{file_name}.csv")
shutil.make_archive(
    base_name=DIR2PAPER_TABLES / "tables_donnees",
    format="zip",
    root_dir=DIR2PAPER_TABLES,
)

# %%
