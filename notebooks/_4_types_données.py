"""
Ce notebook détaille la liste des types de données intégrés dans chaque EDS.

Les données ont été récoltées lors du cycle d'échange sur les entrepôts de données hospitaliers menées par la HAS.

Nous étudions principalement la table `cycle_eds_entrepots.csv`.
"""
# %%
import numpy as np
import pandas as pd
from cycleds.constants import (
    DIR2PAPER_TABLES,
    DIR2REPORTS,
)
from cycleds.data.processing_utils import (
    COL_ENTREPOT_ID,
    COL_INTERVIEW_DATETIME,
    COL_IS_CHU,
    get_entrepots,
    is_chu,
)
from cycleds.plot_utils import save_figure_to_folders

# # Creation de la carte des établissements
# %%
df_entrepots = get_entrepots()
df_entrepots = is_chu(df_entrepots)
# %% Plot invités, type de background et nb établissement
language = "EN"
if language == "EN":
    # look only at CHUs
    df_entrepots = df_entrepots.loc[
        (~df_entrepots["entrepot_state"].isna())
        & (df_entrepots["entrepot_state"] != "no_eds")
        & (df_entrepots[COL_IS_CHU] == True)
        & (df_entrepots["etablissement_rencontre"] == True)
    ]
n_chus_total = df_entrepots[COL_IS_CHU].sum()

mask_with_data_information = (
    df_entrepots["etablissement_rencontre"] == True
) & (
    df_entrepots["entrepot_state"].isin(
        ["production", "experimentation", "prospective"]
    )
)
df_entrepots_w_data = df_entrepots.loc[mask_with_data_information]
df_donnees_eds = df_entrepots_w_data[
    ["organisation", "entrepot_id", "données"]
]
df_donnees_eds["données"] = df_donnees_eds["données"].apply(
    lambda x: x.split(",")
)

if language == "EN":
    label_type_donnees = "Category of data"
    label_count = "Number of HDW"
else:
    label_type_donnees = "Type de données"
    label_count = "Nombre d'EDS"

df_donnees_eds = df_donnees_eds.explode(
    "données",
).rename(columns={"données": label_type_donnees})
n_type_donnees_by_eds = (
    (
        df_donnees_eds.groupby(label_type_donnees)
        .agg(**{label_count: pd.NamedAgg("organisation", "count")})
        .sort_values(label_count, ascending=False)
    )
    .astype("int")
    .reset_index()
)

n_type_donnees_by_eds["Ratio"] = n_type_donnees_by_eds[label_count].apply(
    lambda x: str(int(np.round(100 * x / len(df_entrepots_w_data), 0))) + " %"
)

n_type_donnees_by_eds = n_type_donnees_by_eds.loc[
    n_type_donnees_by_eds[label_count] > 1
]

data_type_en2fr = {
    "Administrative": "GAM",
    "Texts": "Textes",
    "Biology": "Biologie",
    "Drugs": "Circuit du médicament",
    "Imagery": "Imagerie",
    "ICU": "Réanimation",
    "Anatomical pathology": "Anatomopathologie",
    "Medical devices": "Dispositifs médicaux",
    "Billing codes": "PMSI",
    "Emergency": "Urgences",
    "Nurse Forms": "Pancarte infirmière",
}
if language != "EN":
    n_type_donnees_by_eds[label_type_donnees] = n_type_donnees_by_eds[
        label_type_donnees
    ].map(lambda x: data_type_en2fr[x] if x in data_type_en2fr.keys() else x)
n_type_donnees_by_eds.to_excel(DIR2REPORTS / "types_donnees.xlsx", index=False)

with open(DIR2PAPER_TABLES / "types_donnees.tex", "w", encoding="utf8") as f:
    f.write(f"{n_type_donnees_by_eds.to_latex(index=False)}")
n_type_donnees_by_eds
# %%
