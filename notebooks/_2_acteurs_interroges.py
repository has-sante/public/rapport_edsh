"""
Ce notebook détaille la liste des organisations et les compétences des personnes interrogées. Il produit un tableau de résumé à partir de ces informations.

Les données ont été récoltées lors du cycle d'échange sur les entrepôts de données hospitaliers menées par la HAS.

Nous étudions principalement la table `cycle_eds_entrepots.csv` et `cycle_eds_invites.csv`.
"""
# %%
from datetime import datetime
import re
import numpy as np
import pandas as pd
from cycleds.constants import (
    DIR2PAPER_TABLES,
    DIR2REPORTS,
)
from cycleds.data import get_finess
from cycleds.data.processing_utils import (
    COL_ENTREPOT_ID,
    COL_INTERVIEW_DATETIME,
    COL_IS_CHU,
    get_entrepots,
    get_invites,
    is_chu,
)

# # Creation de la carte des établissements
# %%
finess = get_finess()
df_entrepots = get_entrepots()
df_entrepots = is_chu(df_entrepots)
# %% Plot invités, type de background et nb établissement
df_invites = get_invites()

n_chus_total = df_entrepots[COL_IS_CHU].sum()
today = datetime.now()
invites_vus = df_invites.loc[df_invites["Date interview"] <= today]
invites_vus_w_chu_status = is_chu(invites_vus)
n_chus_vus = (
    invites_vus_w_chu_status[["organisation", COL_IS_CHU]]
    .drop_duplicates()[COL_IS_CHU]
    .sum()
)
print(f"Nb de CHUs vu : {n_chus_vus}/{n_chus_total}")

labels_equipes = {
    "Direction de la Recherche": ["drci"],
    "DIM ou Santé Publique": ["dim", "public health"],
    "Direction Données ou Entrepôts": ["data", "entrepôt"],
    "DSI": ["dsi"],
}
n_total_invites = (~df_invites[COL_INTERVIEW_DATETIME].isna()).sum()
print(f"Nombre total d'intervenants: {n_total_invites}")
for label, equipes in labels_equipes.items():
    n_equipe = (
        df_invites["equipe_categorie"].apply(
            lambda x: re.search("|".join(equipes), x.lower()) is not None
        )
    ).sum()
    print(f"{label} : {n_equipe} intervenants")


# %%

# Creation de la liste des organisations et compétences interôgées
summary_table_entrepots = df_entrepots[
    [
        "entrepot_id",
        "organisation",
        "n_etp_entrepot",
        "common_data_model",
        "date_debut",
        "date_debut_cnil",
    ]
].rename(
    columns={
        "n_etp_entrepot": "Nb ETPs dédiés",
        "common_data_model": "Modèles de données",
        "date_debut": "Date Premiers travaux",
        "date_debut_cnil": "Date autorisation CNIL",
    }
)
# 1 ligne par organisation, avec les infos organisationnelles et de l'entrepôt + ajout des compétences des gens intéroggés (sans leurs noms).
# %%
# TODO: needs a bit of work to deduplicate
df_invites = get_invites()
df_invites_entrepots = (
    df_invites.dropna(subset="entrepot_id")
    .assign(
        **{
            "equipe_categorie": lambda df: df["equipe_categorie"].map(
                lambda x: x.split(",")
            )
        }
    )[["equipe_categorie", "entrepot_id"]]
    .explode("equipe_categorie")
)


def count_unique_agg_as_str(col):
    v, nb_v = np.unique(col, return_counts=True)
    agg_str = ",".join([f"{k} : {v}" for k, v in zip(v, nb_v)])
    return agg_str


df_competences_entrepots = (
    df_invites_entrepots.groupby("entrepot_id")
    .agg(**{"": pd.NamedAgg("equipe_categorie", count_unique_agg_as_str)})
    .reset_index()
)


invites_non_hospitaliers = df_invites.loc[df_invites[COL_ENTREPOT_ID].isna()]

df_competences_non_hospitaliers = (
    invites_non_hospitaliers.groupby("organisation")
    .agg(**{"": pd.NamedAgg("equipe_categorie", count_unique_agg_as_str)})
    .reset_index()
    .rename(columns={"organisation": COL_ENTREPOT_ID})
)

df_all_competences = pd.concat(
    [df_competences_entrepots, df_competences_non_hospitaliers], axis=0
)

summary_table_entrepots = summary_table_entrepots.merge(
    df_competences_entrepots,
    on="entrepot_id",
    how="inner",
)

summary_table_entrepots.to_csv(
    DIR2REPORTS / "summary_entrepots.csv", index=False
)
summary_table_entrepots.to_csv(
    DIR2PAPER_TABLES / "summary_entrepots.csv", index=False
)
eds_label = "Health Data Warehouse"
df_all_competences.columns = [
    eds_label,
    "Teams",
]
df_all_competences.to_csv(DIR2REPORTS / "summary_competences.csv", index=False)


# competences only for interviewe CHU at least prospective stage:
list_interviewed_chu_ = invites_vus_w_chu_status[
    [COL_ENTREPOT_ID, COL_IS_CHU]
].drop_duplicates()
list_interviewed_chu_ = list_interviewed_chu_.loc[
    list_interviewed_chu_[COL_IS_CHU] == True
]
# add progression state
list_interviewed_chu_ = list_interviewed_chu_.merge(
    df_entrepots[["entrepot_state", COL_ENTREPOT_ID]],
    on=COL_ENTREPOT_ID,
    how="inner",
)
list_interviewed_chu_ = list_interviewed_chu_.loc[
    (list_interviewed_chu_["entrepot_state"] != "no_eds")
    & (~list_interviewed_chu_["entrepot_state"].isna())
].drop("entrepot_state", axis=1)

df_all_competences_interviewed_CHU = df_all_competences.merge(
    list_interviewed_chu_,
    left_on=eds_label,
    right_on=COL_ENTREPOT_ID,
    how="inner",
).drop([COL_ENTREPOT_ID, COL_IS_CHU], axis=1)

with open(
    DIR2PAPER_TABLES / "summary_competences.tex", "w", encoding="utf8"
) as f:
    f.write(f"{df_all_competences_interviewed_CHU.to_latex(index=False)}")
# TODO: Rajouter  les compétences des startups et institutions interoogées

# %%
# Number of RH for the interviewed teams
summary_table_entrepots.merge(
    list_interviewed_chu_, on=COL_ENTREPOT_ID, how="inner"
)["Nb ETPs dédiés"].describe(percentiles=[0.1, 0.5, 0.9])

# %%
