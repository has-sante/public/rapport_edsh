"""
Ce notebook détaille la localisation géographique et le type des établissments interrogés. Il trace une carte avec ces informations. 

Les données ont été récoltées lors du cycle d'échange sur les entrepôts de données hospitaliers menées par la HAS.

Nous étudions principalement la table `cycle_eds_entrepots.csv` qui référence les .

Nous utilisons également le fichier finess afin de faire des correspondances avec des localisations géographiques.
"""
import re
from matplotlib.lines import Line2D
from cycleds.data import get_finess
from cycleds.data.processing import (
    AVANCEMENT2COLOR,
    ajout_coordonnees_geo,
)
from cycleds.data.processing_utils import get_entrepots, get_invites

import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as ctx
import matplotlib.pyplot as plt

from cycleds.plot_utils import annotate_from_gpd, save_figure_to_folders

# %% [markdown]
# # Creation de la carte des établissements
# Avec [contextily et geopandas](https://linogaliana-teaching.netlify.app/geopandas/)
# %%
finess = get_finess()
df_entrepots = get_entrepots()
geo_df_entrepots = ajout_coordonnees_geo(df_entrepots)
assert geo_df_entrepots.shape[0] == (
    31 + 5
)  # Vérification que l'on a bien les 32 CHUs et les 5 autres centres interrogés
# df_entrepots_w_coordonnees.to_csv(
#     DIR2REPORTS / "entrepots_w_coordonnees.csv", index=False
# )
# Changement du CRS ID récupéré sur le tuto de Lino par le 4326 associé à WGS 84 / système de pseudo-Mercator
geo_df_entrepots = geo_df_entrepots.set_crs(4326)
assert (geo_df_entrepots["interroges"] == "o").sum() == 23
eds_interroges = geo_df_entrepots.loc[geo_df_entrepots["interroges"] == "o"]
nb_eds = geo_df_entrepots["interroges"].shape[0]
print(f"Number of eds in the data (expected:  32 + 5): {nb_eds}")


def organisation2ville(x):
    if x == "HCL":
        return "Lyon"
    elif x == "APHP":
        return "Paris"
    elif x == "APHM":
        return "Marseille"
    elif x == "CHU La Réunion":
        return ""
    else:
        return re.sub("CHU|CHR", "", x).strip()


language_ = "EN"
if language_ == "EN":
    map_label = "ville"
    geo_df_entrepots[map_label] = geo_df_entrepots["organisation"].map(
        lambda x: organisation2ville(x)
    )
else:
    map_label = "organisation"
# %%
fig = plt.figure(figsize=(25, 25))
fig.tight_layout()
ANNOTATION_SIZE = 30
MARKERSIZE = 400
## Creation de sous cartes avec les numéros de régions :
# Ile de france, France, Guadeloupe, Martinique, La Réunion, Corse
# Aide sur [matplotlib](https://matplotlib.org/stable/tutorials/intermediate/arranging_axes.html)
# Need a more complex layout : https://matplotlib.org/stable/tutorials/intermediate/constrainedlayout_guide.html
spec = fig.add_gridspec(4, 4, hspace=0, wspace=0)
ax_idf = fig.add_subplot(spec[0, 3:])
geo_df_idf = geo_df_entrepots.loc[
    geo_df_entrepots["libelle_region"] == "ILE DE FRANCE"
]

marker_col = "interroges"
# Tout les EDS dans la table ont été intérrogés en région parisienne
geo_df_idf.plot(
    ax=ax_idf,
    color=geo_df_idf["color"],
    edgecolor="black",
    marker="o",
    linewidth=1,
    markersize=MARKERSIZE,
)
annotate_from_gpd(
    ax_idf, g_df=geo_df_idf, label_column=map_label, size=ANNOTATION_SIZE
)

# filter out (out-of-metropole + IDF)
ax_fr = fig.add_subplot(
    spec[:3, :3],
)
geo_df_metropole = geo_df_entrepots.loc[
    ~geo_df_entrepots["libelle_region"].isin(
        ["ILE DE FRANCE", "CORSE", "GUADELOUPE", "MARTINIQUE", "LA REUNION"]
    )
]
for marker_shape_ in geo_df_metropole[marker_col].unique():
    geo_df_metropole_interroges = geo_df_metropole.loc[
        geo_df_metropole[marker_col] == marker_shape_
    ]
    geo_df_metropole_interroges.plot(
        ax=ax_fr,
        color=geo_df_metropole_interroges["color"],
        edgecolor="black",
        linewidth=1,
        markersize=MARKERSIZE,
        marker=marker_shape_,
    )
annotate_from_gpd(
    ax_fr,
    g_df=geo_df_metropole,
    label_column=map_label,
    size=ANNOTATION_SIZE,
)

grid_specs = [0, 1, 2, 3]
out_metropole_region_labels = [
    "GUADELOUPE",
    #  "LA MARTINIQUE",
    "LA REUNION",
    # "CORSE",
]  # bug avec corse
# Personne n'a encore été interrogés en dehors de la métropole
ax_out_metropole_regions = []
for region_label_, g_spec_ in zip(out_metropole_region_labels, grid_specs):
    ax_region = fig.add_subplot(spec[3, g_spec_])
    geo_df_region = geo_df_entrepots.loc[
        geo_df_entrepots["libelle_region"] == region_label_
    ]

    geo_df_region.plot(
        ax=ax_region,
        color=geo_df_region["color"],
        edgecolor="black",
        linewidth=1,
        markersize=MARKERSIZE,
        marker="X",
    )
    annotate_from_gpd(
        ax_region,
        g_df=geo_df_region,
        label_column=map_label,
        size=ANNOTATION_SIZE,
    )
    ax_out_metropole_regions.append(ax_region)


all_axes = [ax_fr, ax_idf] + ax_out_metropole_regions

axes_limits = [
    (-5.132778, 8.5, 41.5, 51.409516),
    (2.20, 2.45482, 48.80, 48.92),
    (-62.09, -60.92, 15.83, 16.58),
    (-61.32, -60.69, 14.35, 14.9),
    (55.057, 55.9, -21.43, -20.839),
    (7.95, 9.95, 41.33183, 43.07816),
]
axes_zoom = [7, 12, 8, 8, 8, 7]

for ax, map_lim, map_zoom in zip(all_axes, axes_limits, axes_zoom):
    ax.set(xticks=[], yticks=[])

    if map_lim is not None:
        ax.set_xlim((map_lim[0], map_lim[1]))
        ax.set_ylim((map_lim[2], map_lim[3]))
    ctx.add_basemap(
        ax,
        crs=geo_df_entrepots.crs.to_string(),
        zoom=map_zoom,
        attribution=False,
        reset_extent=True,
        source=ctx.providers.OpenStreetMap.Mapnik,  # CartoDB.Voyager # GeoportailFrance.plan,
    )
    # All sources are available https://contextily.readthedocs.io/en/latest/intro_guide.html
    ax.set_aspect("auto")

legend_marker_size = 30
handles = []
labels = []
for _, avancement_dic in AVANCEMENT2COLOR.items():

    handles.append(
        Line2D(
            [0],
            [0],
            color=avancement_dic["color"],
            marker="o",
            markersize=legend_marker_size,
            linewidth=1,
            linestyle="None",
            markeredgecolor="black",
        )
    )
    labels.append(avancement_dic["label"])
handles.extend(
    [
        Line2D(
            [0],
            [0],
            color="white",
            marker="o",
            markersize=legend_marker_size,
            linewidth=1,
            linestyle="None",
            markeredgecolor="white",
        ),
        Line2D(
            [0],
            [0],
            color="black",
            marker="o",
            markersize=legend_marker_size,
            linewidth=1,
            linestyle="None",
            markeredgecolor="black",
        ),
        Line2D(
            [0],
            [0],
            color="black",
            marker="X",
            markersize=34,
            linewidth=1,
            linestyle="None",
            markeredgecolor="black",
        ),
    ]
)
labels.extend(["Acteurs rencontrés", "Rencontré", "Non rencontré"])
ax_leg = fig.add_subplot(spec[1, 3])
ax_leg.add_artist(
    plt.legend(handles, labels, title="Etat d'avancement", ncol=1)
)
ax_leg.set(xticks=[], yticks=[])
ax_leg.spines["top"].set_visible(False)
ax_leg.spines["right"].set_visible(False)
ax_leg.spines["bottom"].set_visible(False)
ax_leg.spines["left"].set_visible(False)
ax_leg.grid(False)
ax_idf.spines["left"].set_color("black")

save_figure_to_folders("cartography")

# %%
