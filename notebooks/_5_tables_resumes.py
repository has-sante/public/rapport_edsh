"""
Ce notebook détaille les colonnes disponibles dans les 3 tables utilisées pour le rapport sur les EDS.

Nous étudions principalement les tables `cycle_eds_entrepots.csv` et `cycle_eds_invites.csv` et `cycle_eds_etudes.csv` 
"""
# %%
from cycleds.data.processing_utils import (
    get_entrepots,
    get_invites,
    get_etudes,
)

# # Creation de la carte des établissements
# %%
tables_resultats = {
    "Invités": get_invites(),
    "Entrepôts": get_entrepots(),
    "Etudes": get_etudes(),
}
bold_str_b = "\033[1m"
bold_str_e = "\033[0m"
print("Colonnes renseignées dans chaque table :\n")
for table_name, table in tables_resultats.items():
    print(
        f"\n {bold_str_b}- Table {table_name} :{bold_str_e}  {', '.join(table.columns)}"
    )

# %%
