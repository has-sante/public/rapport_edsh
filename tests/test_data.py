import numpy as np
import pandas as pd
import pytest
from cycleds.data.processing import ajout_sae_n_sejours, ajout_coordonnees_geo
from cycleds.data.processing_utils import get_entrepots


@pytest.fixture()
def df_entrepots():
    df_entrepots = get_entrepots()
    return df_entrepots


@pytest.fixture()
def df_entrepots_w_sae(df_entrepots):
    return ajout_sae_n_sejours(df_entrepots)


def test_aggregation_by_ej(df_entrepots_w_sae):
    # L'entrepôt avec le plus de séjours devrait tout le temps être l'EDS_APHP.
    df_entrepots_sorted_by_stays = df_entrepots_w_sae.sort_values(
        "n_sejours_comptes", ascending=False
    )
    assert df_entrepots_sorted_by_stays["entrepot_id"].values[0] == "EDS_APHP"


def test_ajout_sae_n_sejours(df_entrepots, df_entrepots_w_sae):
    sorted_initial_entrepots = np.sort(df_entrepots["entrepot_id"])
    sorted_processed_entrepots = np.sort(df_entrepots_w_sae["entrepot_id"])
    assert np.array_equal(sorted_initial_entrepots, sorted_processed_entrepots)


def test_ajout_coordonnees_geo(df_entrepots):
    coordonnes_geo = ajout_coordonnees_geo(df_entrepots)

    assert df_entrepots.shape[0] == coordonnes_geo.shape[0]
