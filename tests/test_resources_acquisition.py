import numpy as np
from cycleds.data.resources_acquisition import download_specialites_snsd


def test_download_specialites_snds():
    specialites = download_specialites_snsd(save=False)
    assert np.array_equal(
        specialites.columns,
        [
            "PFS_SPA_COD",
            "snds_specialite_label",
            "local_specialite_label",
        ],
    )
