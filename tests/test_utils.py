import numpy as np
import pytest
import pandas as pd
from cycleds.utils import get_closest_levenstein, get_topics_in_project

targeted_terms = {
    "covid": ["covid-19", "covid19", "covid"],
}
dummy_project = pd.DataFrame(
    {
        "Titre.projet": ["Cardio", "Covid19", "Pneumo"],
        "Demarche.suivie": ["cov3Id1", "hello", "42"],
    }
)


@pytest.mark.parametrize("topic, topic_dict", targeted_terms.items())
def test_get_topics_in_project(topic, topic_dict):
    found_topics = get_topics_in_project(dummy_project, topic, topic_dict)
    assert found_topics["Titre.projet"].values[0] == "Covid19"
    assert len(found_topics) == 1


def test_get_closest():
    quandidates = ["bouteille", "kangourou", "kagoro"]
    expected = ["kangourou", "kagoro", "bouteille"]
    w = "kangourou"
    best_quandidates = get_closest_levenstein(w, quandidates)
    assert np.array_equal(best_quandidates, expected)
