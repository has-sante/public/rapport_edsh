# Données et code du rapport EDSH 

📃 [English version](README_english.md)

Ce dépôt de code contient les données et le code permettant de reproduire les figures du rapport sur les [entrepôts de données de santé publié par la HAS](https://www.has-sante.fr/jcms/p_3386076/fr/entrepots-de-donnees-de-sante-hospitaliers-la-has-publie-un-panorama-inedit-en-france) en novembre 2022. 

Une publication en anglais dans la revue Plos digital health a également été publié en 2023: [Good practices for clinical data warehouse implementation: A case study in France](https://journals.plos.org/digitalhealth/article?id=10.1371/journal.pdig.0000298). Le fichier contenant le code source de cette publication est disponible dans ce dépôt : `publication-plos-edsh.zip`.

## Données 

Trois tables sont associées au rapport : 

- La table des **invités** concerne les individus interrogés, les dates d’entretien, les postes et l’appartenance à une équipe spécifique.
- La table des **entrepôts** collige des informations sur les EDSH. 
- La table des **études** est le référencement des études renseignés sur 10 portails d’études en cours (ou terminées si celles-ci sont disponibles) disponible en accès libre.

Colonnes renseignées dans chaque table :

- [**Table Invités :**](data/cycle_eds/cycle_eds_intervenants.csv)  Nom, Date interview, organisation, Poste, equipe_categorie, entrepot_id
- [**Table Entrepôts :**](data/cycle_eds/cycle_eds_entrepots.csv)  entrepot_id, organisation, entrepot_state, url_portail_etudes_en_cours, n_etp_entrepot, n_patients, n_etudes_en_cours, common_data_model, url_procedures_access, datalab, doc_cyle_de_vie_donnee, equipe_qualite, etablissement_rencontre, date_debut, date_debut_cnil, données, deidentification, valorisation_text_col, DPI, structure_administrative, private_actors, cycle_eds_invites, id_fi_ej, id_fi
- [**Table Etudes :**](data/cycle_eds/cycle_eds_etudes.csv)  titre_etude, titre_etude_long, type_etude, methode_etude, domaine_medical, covid, objectif, date_collecte, entrepot_id, source

## Répliquer les résultats

### Installation

Le projet utilise python et des fonctions utilitaires présentes dans le dossier [`cycleds`](cycleds/). Pour installer ces fonctions et les autres dépendences python nécessaires, il est possible d'utiliser `poetry` (méthode conseillée, avec [guide d'installation ici](https://python-poetry.org/docs/#installation)) ou `pip` : 

- Poetry :
```
cd rapport_edsh
poetry install
```

- pip : 
```
cd rapport_edsh
pip install -e .
```

### Utilisation des notebooks pour répliquer les résultats

Chaque notebook créé différents types de résultats présentées dans le rapport. Afin d'obtenir les figures et tableaux, lancer simplement `python <notebook_path>`. Les résultats sont sauvegardés dans le dossier [`reports`](reports/). Afin de voir les résultats de manière interactive, ouvrir les notebooks avec un [jupyter](notebooks/README.md). 

Contenu des notebooks: 

- [`_0_types_specialites_etudes.py`](notebooks/_0_types_specialites_etudes.py): Ce notebook détaille les types et les méthodologies des études menées sur les entrepôts de données de santé. 

- [`_1_timeline_eds.py`](notebooks/_1_timeline_eds.py) : Ce notebook détaille l'historique de la création des entrepôts en traçant un graphique de type timeline.


- [`_2_acteurs_interroges.py`](notebooks/_2_acteurs_interroges.py) : Ce notebook détaille la liste des organisations et les compétences des personnes interrogées. Il produit un tableau de résumé à partir de ces informations.


- [`_3_map.py`](notebooks/_3_map.py) : Ce notebook détaille la localisation géographique et le type des établissments interrogés. Il trace une carte avec ces informations. 


- [`_4_types_données.py`](notebooks/_4_types_donn%C3%A9es.py) : Ce notebook détaille la liste des types de données intégrés dans chaque EDS.


- [`_5_tables_resumes.py`](notebooks/_5_tables_resumes.py) : Ce notebook détaille les colonnes disponibles dans les 3 tables utilisées pour le rapport sur les EDS.
