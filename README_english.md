# EDSH report data and code 

📃 [English version](README_english.md)

This code repository contains the data and code to reproduce the figures from the report on [health data warehouses published by the HAS]() in November 2022. 

## Data 

Three tables are associated with the report: 

- The **guests** table is for individuals interviewed, interview dates, positions, and membership in a specific team.
- The **storage** table collects information about the HDS. 
- The **studies** table is the referencing of studies reported on 10 current (or completed if available) study portals available for free access.

Columns filled in each table:

- **[Guests table](data/cycle_eds/cycle_eds_intervenants.csv):** Name, Interview date, organization, position, team_category, warehouse_id
- **[Warehouse table](data/cycle_eds/cycle_eds_entrepots.csv):** entrepot_id, organization, entrepot_state, url_portal_in_course_studies, n_etp_entrepot, n_patients, n_in_course_studies, common_data_model, url_procedures_access, datalab, doc_cyle_of_life_data, team_quality, etablissement_rencontre, date_debut, date_debut_cnil, data, deidentification, valorisation_text_col, DPI, structure_administrative, private_actors, cycle_eds_invites, id_fi_ej, id_fi
- **[Studies table](data/cycle_eds/cycle_eds_etudes.csv):** study_title, study_title_long, study_type, study_method, medical_field, covid, objective, collection_date, warehouse_id, source

## Replicate results

### Installation

The project uses python and utility functions found in the [`cycleds`](cycleds/) folder. To install these functions and other necessary python dependencies, it is possible to use `poetry` (recommended method, with [installation guide here](https://python-poetry.org/docs/#installation)) or `pip` : 

- Poetry :
```
cd rapport_edsh
poetry install
```

- pip : 
```
cd rapport_edsh
pip install -e .
```

### Using notebooks to replicate results

Each notebook creates different types of results presented in the report. In order to get the figures and tables, simply run `python <notebook_path>`. The results are saved in the [`reports`](reports/) folder. In order to see the results interactively, open the notebooks with [jupyter](notebooks/README.md). 

Contents of the notebooks: 

- [`_0_types_specialites_etudes.py`](notebooks/_0_types_specialites_etudes.py): This notebook details the types and methodologies of studies conducted on health data warehouses. 

- [`_1_timeline_eds.py`](notebooks/_1_timeline_eds.py): This notebook details the history of the creation of the warehouses by drawing a timeline type graph.


- `_2_actors_interviews.py`](notebooks/_2_actors_interviews.py) : This notebook details the list of organizations and the skills of the interviewees. It produces a summary table from this information.


- [`3_map.py`](notebooks/_3_map.py) : This notebook details the geographical location and type of the interviewed establishments. It draws a map with this information. 


- [`_4_data_types.py`](notebooks/_4_data_types%C3%A9es.py) : This notebook details the list of data types integrated in each DHS.


- [`_5_tables_resumes.py`](notebooks/_5_tables_resumes.py) : This notebook details the columns available in the 3 tables used for the DHS report.
